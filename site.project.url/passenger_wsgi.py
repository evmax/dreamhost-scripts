import sys, os

home_path = os.environ['HOME']
os.environ['LD_LIBRARY_PATH'] = os.path.join(home_path, 'Python35/lib')
username = os.environ['USER']
virtualenv_name = 'env'
project_name = 'project'
app_dir_name = 'site.project.ru'
python_ver = 'python3.5'

settings_module = '%s.core.settings' % project_name
venv_path = os.path.join(home_path, '.virtualenvs', virtualenv_name)
site_packages = os.path.join(venv_path, 'lib', python_ver, 'site-packages', project_name)
sys.path.append(site_packages)

# using python from env will magically allow using site-packages
INTERP = os.path.join(venv_path, "bin", "python3.5")

if sys.executable != INTERP:
    os.execl(INTERP, INTERP, *sys.argv)

os.environ['DJANGO_SETTINGS_MODULE'] = settings_module

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()