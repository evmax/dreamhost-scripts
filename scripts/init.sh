cd ~
mkdir tmp
cd tmp
wget https://www.python.org/ftp/python/3.5.6/Python-3.5.6.tgz
tar zxvf Python-3.5.6.tgz
cd Python-3.5.6
./configure --enable-shared --prefix=$HOME/Python35
make install

export PATH="$HOME/Python35/bin:$PATH"
export LD_LIBRARY_PATH=$HOME/Python35/lib

# save it and run
source ~/.bashrc

# pip and virtualenvwrapper
cd ~/tmp
curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
python3.5 get-pip.py

export WORKON_HOME=$HOME/.virtualenvs
mkdir $WORKON_HOME
pip3.5 install --user virtualenvwrapper
export VIRTUALENVWRAPPER_PYTHON=$HOME/Python35/bin/python3.5
source $HOME/.local/bin/virtualenvwrapper.sh


mkvirtualenv env